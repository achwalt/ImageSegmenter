import torch
import torch.nn as nn

import torchvision
import torchvision.transforms
import torchvision.transforms.functional as tf


class DoubleConv2d(nn.Module):

    def __init__(self, channels_in, channels_out, kernel_size = 3, strides = 1, padding = 1):
        # Delegate all methods to nn.Module
        super(DoubleConv2d, self).__init__()

        self.conv2d = nn.Sequential(
            # Strides and padding to 1 to retain image size (Same Convolution)
            nn.Conv2d(channels_in, channels_out, kernel_size, strides, padding, bias=False),
            
            # Batch Normalization to increase speed and stability
            # Potentially induces severe gradient explosion
            nn.BatchNorm2d(channels_out),
            nn.ReLU(inplace = True),
            nn.Conv2d(channels_out, channels_out, kernel_size, strides, padding, bias = False),
            nn.BatchNorm2d(channels_out),
            nn.ReLU(inplace = True)
        )

    def forward(self, x):
        # To adhere with PyTorch standards
        return self.conv2d(x)


class UNet(nn.Module):

    def __init__(
        self,
        channels_in = 3,
        channels_out = 1,
        features = [64, 128, 256, 512],
        maxpool_kernel_size = 2,
        maxpool_stride = 2
    ):
        super(UNet, self).__init__()

        # Number of 'upward' movements on the right U-Leg and downward movements on the left
        self.downward = nn.ModuleList()
        self.upward = nn.ModuleList()
        
        self.pool = nn.MaxPool2d(kernel_size = maxpool_kernel_size, stride = maxpool_stride)

        # Moving down the left U-Leg
        # TODO: Pass config for each DoubleConv2d
        for nr_channels_out in features:
            self.downward.append(DoubleConv2d(
                channels_in=channels_in,
                channels_out=nr_channels_out
            ))
            channels_in = nr_channels_out

        # Moving up the right U-Leg
        # TODO: Pass config as well
        for nr_channels_out in features[::-1]:
            self.upward.append(
                nn.ConvTranspose2d(
                    nr_channels_out * 2,
                    nr_channels_out,
                    kernel_size = 2,
                    stride = 2 # Double image size
                )
            )
            self.upward.append(
                DoubleConv2d(
                    channels_in = nr_channels_out * 2,
                    channels_out = nr_channels_out
                )
            )

        self.connecting_layer = DoubleConv2d(
            channels_in = features[-1], 
            channels_out = features[-1] * 2
        )

        self.output_layer = nn.Conv2d(
            features[0], 
            channels_out, 
            kernel_size = 1
        )

    
    def forward(self, x):
        translucent_connections = []

        for layer in self.downward:
            x = layer(x)
            translucent_connections.append(x)
            x = self.pool(x)

        x = self.connecting_layer(x)
        
        # Reverse connections for moving up
        translucent_connections = translucent_connections[::-1]

        # Up -> DoubleConv2D -> Up -> ...
        for i in range(0, len(self.upward), 2):
            x = self.upward[i](x)
            trans_con = translucent_connections[i//2]

            # If size of input images is not divisible by 2
            if x.shape != trans_con.shape:
                x = tf.resize(x, size = trans_con.shape[2:])
            
            concat_con = torch.cat((trans_con, x), dim = 1)
            x = self.upward[i+1](concat_con)

        return self.output_layer(x)


# Test me!
if __name__ == "__main__":
    x = torch.randn((3, 1, 320, 320))
    model = UNet(channels_in=1, channels_out=1)
    predictions = model(x)
    assert predictions.shape == x.shape